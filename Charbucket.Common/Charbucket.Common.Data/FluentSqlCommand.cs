﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Charbucket.Common.Logging;

namespace Charbucket.Common.Data
{
    public class FluentSqlCommand
    {
        private readonly SqlParameterCollection _parameters;
        private bool _leaveConnectionOpened;
        private bool _includeReturnValue;
        private bool _isSafe;
        private bool _withLog;
        private Action<SqlDataReader> _dataReaderAction;

        private readonly SqlCommand _command;
        private static Log _log;
        private static Log Logger
        {
            get
            {
                if (_log != null) return _log;
                _log = Log.For<FluentSqlCommand>();
                return _log;
            }
        }

        public ExecutionStatus ExecutionStatus { get; private set; }
        public SqlParameter ReturnValue { get; private set; }

        public FluentSqlCommand()
        {
            _command = new SqlCommand();
            _parameters = _command.Parameters;
            ExecutionStatus = ExecutionStatus.NotExecuted;
        }

        public FluentSqlCommand WithConnection(SqlConnection connection, bool leaveOpened = false)
        {
            _command.Connection = connection;
            _leaveConnectionOpened = leaveOpened;
            return this;
        }

        public FluentSqlCommand WithConnection(string connectionString, bool leaveOpened = false)
        {
            return WithConnection(new SqlConnection(connectionString), leaveOpened);
        }

        public FluentSqlCommand WithTimeout(int timeout)
        {
            _command.CommandTimeout = timeout;
            return this;
        }

        public FluentSqlCommand WithReturnValue()
        {
            _includeReturnValue = true;
            return this;
        }

        public FluentSqlCommand Safe()
        {
            _isSafe = true;
            return this;
        }

        public FluentSqlCommand WithLog()
        {
            _withLog = true;
            return this;
        }

        public FluentSqlCommand Procedure(string procedureName)
        {
            _command.CommandText = procedureName;
            _command.CommandType = CommandType.StoredProcedure;
            return this;
        }

        public FluentSqlCommand Query(string queryText)
        {
            _command.CommandText = queryText;
            _command.CommandType = CommandType.Text;
            return this;
        }

        public FluentSqlCommand WithParameter(string name, object value)
        {
            _parameters.AddWithValue(name, value);
            return this;
        }

        public FluentSqlCommand WithParameterIfNotEmpty(string name, object value)
        {
            if (value != null)
            {
                _parameters.AddWithValue(name, value);
            }

            return this;
        }
        public FluentSqlCommand WithParameterIfNotEmpty(string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                _parameters.AddWithValue(name, value);
            }

            return this;
        }

        #region Async methods

        public async Task<int> ExecuteNonQueryAsync()
        {
            var result = await ExecuteAsync(ExecutionType.NonQuery);
            return result.NonQueryResult;
        }

        public async Task<object> ExecuteScalarAsync()
        {
            var result = await ExecuteAsync(ExecutionType.Scalar);
            return result.ScalarResult;
        }

        public async Task<IDataReader> ExecuteReaderAsync()
        {
            InitializeCommand();
            AddReturnParameter();
            return await _command.ExecuteReaderAsync();
        }

        public async void ExecuteReaderAsync(Action<SqlDataReader> action)
        {
            _dataReaderAction = action;
            await ExecuteAsync(ExecutionType.Reader);
        }

        private async Task<ExecutionResult> ExecuteAsync(ExecutionType type)
        {
            var result = new ExecutionResult();

            using (_leaveConnectionOpened ? null : _command.Connection)
            {
                try
                {
                    InitializeCommand();
                    using (_command)
                    {
                        AddReturnParameter();

                        switch (type)
                        {
                            case ExecutionType.NonQuery:
                                result.NonQueryResult = await _command.ExecuteNonQueryAsync();
                                break;
                            case ExecutionType.Scalar:
                                result.ScalarResult = await _command.ExecuteScalarAsync();
                                break;
                            case ExecutionType.Reader:
                                var reader = await _command.ExecuteReaderAsync();
                                using (reader)
                                {
                                    do
                                    {
                                        while (reader.Read())
                                        {
                                            _dataReaderAction?.Invoke(reader);
                                        }
                                    } while (reader.NextResult());
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (_withLog)
                    {
                        Logger.Error(ex);
                    }
                    if (!_isSafe)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        #endregion

        #region Sync methods

        public int ExecuteNonQuery()
        {
            return Execute(ExecutionType.NonQuery).NonQueryResult;
        }

        public object ExecuteScalar()
        {
            return Execute(ExecutionType.Scalar).ScalarResult;
        }

        public IDataReader ExecuteReader()
        {
            InitializeCommand();
            AddReturnParameter();
            return _command.ExecuteReader();
        }

        public void ExecuteReader(Action<SqlDataReader> action)
        {
            _dataReaderAction = action;
            Execute(ExecutionType.Reader);
        }

        private ExecutionResult Execute(ExecutionType type)
        {
            var result = new ExecutionResult();

            using (_leaveConnectionOpened ? null : _command.Connection)
            {
                try
                {
                    InitializeCommand();
                    using (_command)
                    {
                        AddReturnParameter();

                        switch (type)
                        {
                            case ExecutionType.NonQuery:
                                result.NonQueryResult = _command.ExecuteNonQuery();
                                break;
                            case ExecutionType.Scalar:
                                result.ScalarResult = _command.ExecuteScalar();
                                break;
                            case ExecutionType.Reader:
                                var reader = _command.ExecuteReader();
                                using (reader)
                                {
                                    do
                                    {
                                        while (reader.Read())
                                        {
                                            if (_dataReaderAction != null)
                                            {
                                                _dataReaderAction(reader);
                                            }
                                        }
                                    } while (reader.NextResult());
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (_withLog)
                    {
                        Logger.Error(ex);
                    }
                    if (!_isSafe)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        #endregion

        private void InitializeCommand()
        {
            if (_command.Connection.State != ConnectionState.Open)
            {
                _command.Connection.Open();
            }
        }

        private void AddReturnParameter()
        {
            if (_includeReturnValue)
            {
                ReturnValue = new SqlParameter("__returnValue", SqlDbType.Int)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                _parameters.Add(ReturnValue);
            }
        }

        private class ExecutionResult
        {
            public object ScalarResult { get; set; }
            public int NonQueryResult { get; set; }
        }

        private enum ExecutionType
        {
            NonQuery,
            Scalar,
            Reader
        }
    }
}
