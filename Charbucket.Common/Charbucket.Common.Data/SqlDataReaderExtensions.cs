﻿using System;
using System.Data.SqlClient;

namespace Charbucket.Common.Data
{
    public static class SqlDataReaderExtensions
    {
        public static Int32 GetInt32(this SqlDataReader reader, string name)
        {
            return reader.GetInt32(reader.GetOrdinal(name));
        }

        public static Int32? GetInt32Null(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (Int32?)null : reader.GetInt32(ordinal);
        }

        public static Int16 GetInt16(this SqlDataReader reader, string name)
        {
            return reader.GetInt16(reader.GetOrdinal(name));
        }

        public static Int16? GetInt16Null(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (Int16?)null : reader.GetInt16(ordinal);
        }

        public static string GetString(this SqlDataReader reader, string name)
        {
            return reader.GetString(reader.GetOrdinal(name));
        }

        public static string GetStringNull(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
        }

        public static byte GetByte(this SqlDataReader reader, string name)
        {
            return reader.GetByte(reader.GetOrdinal(name));
        }

        public static byte? GetByteNull(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (byte?) null : reader.GetByte(ordinal);
        }

        public static DateTime GetDateTime(this SqlDataReader reader, string name)
        {
            return reader.GetDateTime(reader.GetOrdinal(name));
        }

        public static DateTime? GetDateTimeNull(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (DateTime?)null : reader.GetDateTime(ordinal);
        }

        public static bool GetBoolean(this SqlDataReader reader, string name)
        {
            return reader.GetBoolean(reader.GetOrdinal(name));
        }

        public static bool? GetBooleanNull(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (bool?) null : reader.GetBoolean(ordinal);
        }

        public static decimal GetDecimal(this SqlDataReader reader, string name)
        {
            return reader.GetDecimal(reader.GetOrdinal(name));
        }

        public static decimal? GetDecimalNull(this SqlDataReader reader, string name)
        {
            var ordinal = reader.GetOrdinal(name);
            return reader.IsDBNull(ordinal) ? (decimal?) null : reader.GetDecimal(name);
        }
    }
}
