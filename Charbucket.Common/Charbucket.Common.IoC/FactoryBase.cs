﻿
using System;
using System.Linq;
using Autofac;
using Autofac.Builder;

namespace Charbucket.Common.IoC
{
    public class FactoryBase
    {
        #region Singleton

        private static readonly FactoryBase MainInstance;

        static FactoryBase()
        {
            MainInstance = new FactoryBase();
        }

        public static FactoryBase Instance
        {
            get { return MainInstance; }
        }

        #endregion

        private static readonly ContainerBuilder Builder = new ContainerBuilder();
        private static readonly Lazy<IContainer> Container = new Lazy<IContainer>(() => Builder.Build());

        protected virtual void Initialize()
        {
        }

        public ContainerBuilder GetBuilder()
        {
            return Builder;
        }

        public IContainer GetContainer()
        {
            return Container.Value;
        }

        public virtual T Create<T>(params object[] args)
            where T : class
        {
            return Container.Value.Resolve<T>(args.Select((x, index) => new PositionalParameter(index, x)));
        }

        public IRegistrationBuilder<TClass, SimpleActivatorData, SingleRegistrationStyle> Register<TInterface, TClass>()
            where TClass : TInterface, new()
        {
            return Builder.Register((context, parameters) => new TClass()).As<TInterface>().ExternallyOwned();
        }

        public void RegisterType
            <TInterface, TClass>()
        {
            Builder.RegisterType<TClass>().As<TInterface>().ExternallyOwned();
        }

        public IRegistrationBuilder<TInterface, SimpleActivatorData, SingleRegistrationStyle> Register<TInterface>(Func<TInterface> creationFunc)
        {
            return Builder.Register((context, parameters) => creationFunc()).As<TInterface>().ExternallyOwned();
        }

        public void RegisterSingleInstance<TInterface>(Func<TInterface> creationFunc)
        {
            Builder.Register((context, parameters) => creationFunc()).As<TInterface>().ExternallyOwned().SingleInstance();
        } 

        public IRegistrationBuilder<TInterface, SimpleActivatorData, SingleRegistrationStyle> Register<TInterface>(Type interfaceType, Func<TInterface> func)
        {
            return Builder.Register((context, parameters) => func()).As(interfaceType).ExternallyOwned();
        } 
    }
}
