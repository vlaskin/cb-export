﻿using NLog;
using System;

namespace Charbucket.Common.Logging
{
    public class Log
    {
        private readonly Logger _logger;

        private Log(string name)
        {
            _logger = LogManager.GetLogger(name);
        }

        private Log()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public static Log For(string name)
        {
            return new Log(name);
        }

        public static Log For(Type type)
        {
            return new Log(type.Name);
        }

        public static Log For<T>()
        {
            return For(typeof(T));
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Debug(string message, Exception exception)
        {
            _logger.Debug(exception, message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Info(string message, Exception exception)
        {
            _logger.Info(exception, message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Warn(string message, Exception exception)
        {
            _logger.Warn(exception, message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception exception)
        {
            _logger.Error(exception);
        }
    }
}
