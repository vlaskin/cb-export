﻿using System;
using System.Text;

namespace Charbucket.Common.Extensions
{
    public static class ArrayExtensions
    {
        public static string ToHex(this byte[] bytes, bool upperCase)
        {
            var result = new StringBuilder(bytes.Length * 2);

            foreach (var t in bytes)
                result.Append(t.ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }

        public static T[] LeftShift<T>(this T[] array, int offset)
        {
            var newArray = new T[array.Length - offset];
            Array.Copy(array, offset, newArray, 0, array.Length - offset);
            return newArray;
        }
    }
}
