﻿using System.Collections.Generic;

namespace Charbucket.Common.Extensions
{
    public static class DictionaryExtenstions
    {
        public static bool ContainsKeys<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys)
        {
            var res = false;
            foreach (var key in keys)
            {
                res = dictionary.ContainsKey(key);
                if (res)
                {
                    break;
                }
            }
            return res;
        }
    }
}
