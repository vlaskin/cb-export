﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Charbucket.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string value)
        {
            return String.IsNullOrEmpty(value);
        }

        public static string F(this string format, params object[] args)
        {
            return String.Format(format, args);
        }

        public static string GetMd5Hash(this string value)
        {
            string hash;
            using (var md5 = MD5.Create())
            {
                hash = md5.ComputeHash(Encoding.UTF8.GetBytes(value)).ToHex(false);
            }
            return hash;
        }

        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString(CultureInfo.InvariantCulture).ToUpper() + input.Substring(1);
        }

        public static string IfEmptyThenNull(this string input)
        {
            return input.IsNullOrEmpty() ? null : input;
        }
    }
}
