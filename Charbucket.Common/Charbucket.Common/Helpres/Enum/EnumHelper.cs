﻿using System.Collections.Generic;
using System.Linq;
using System.Resources;

namespace Charbucket.Common.Helpres.Enum
{
    public static class EnumHelper
    {
        public static IEnumerable<LocalizedEnumMember> GetLocalizedEnumMembers<TEnum>(ResourceSet resourceSet)
        {
            var names = System.Enum.GetNames(typeof (TEnum));
            var values = System.Enum.GetValues(typeof (TEnum));

            return names.Select((t, i) => new LocalizedEnumMember(i, t, resourceSet.GetString(t))).ToList();
        } 
    }
}
