﻿namespace Charbucket.Common.Helpres.Enum
{
    public sealed class LocalizedEnumMember
    {
        public object Value { get; }
        public string Name { get; }
        public string LocalizedName { get; }

        public LocalizedEnumMember(object value, string name, string localizedName)
        {
            Value = value;
            Name = name;
            LocalizedName = localizedName;
        }
    }
}
