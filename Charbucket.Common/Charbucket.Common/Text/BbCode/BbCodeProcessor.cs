﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Charbucket.Common.Text.BbCode
{
    public sealed class BbCodeProcessor
    {
        private readonly IEnumerable<BbTag> _tags;

        public BbCodeProcessor(IEnumerable<BbTag> tags)
        {
            _tags = tags;
        }

        public string ToHtml(string bbCodeText, bool handleNewLines = true)
        {
            if (string.IsNullOrWhiteSpace(bbCodeText))
            {
                return bbCodeText;
            }

            var result = bbCodeText;

            // handle new lines if needed
            if (handleNewLines)
            {
                var newLinesRegex = new Regex("^(.*)$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                result = newLinesRegex.Replace(result, "<p>$1</p>\n\r");
            }

            //handle tags
            foreach (var bbTag in _tags)
            {
                if (string.IsNullOrWhiteSpace(bbTag.HtmlContainerAttribute))
                {
                    var regex = new Regex($@"\[{bbTag.BbName}.*?\](.*?)\[/{bbTag.BbName}\]",
                        RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    result = regex.Replace(result, $"<{bbTag.HtmlTag}>$1</{bbTag.HtmlTag}>");
                }
                else
                {
                    var regex = new Regex($@"\[{bbTag.BbName}=(.*?)\](.*?)\[/{bbTag.BbName}\]",
                        RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    result = regex.Replace(result, $"<{bbTag.HtmlTag} {bbTag.HtmlContainerAttribute}=\"$1\">$2</{bbTag.HtmlTag}>");
                }
            }

            return result;
        }
    }
}
