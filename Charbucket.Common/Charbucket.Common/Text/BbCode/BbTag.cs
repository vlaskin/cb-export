﻿namespace Charbucket.Common.Text.BbCode
{
    public sealed class BbTag
    {
        public string BbName { get; }
        public string HtmlTag { get; }

        public string HtmlContainerAttribute { get; }

        public BbTag(string bbName, string htmlOpenTag, string htmlContainerAttribute = null)
        {
            BbName = bbName;
            HtmlTag = htmlOpenTag;

            HtmlContainerAttribute = htmlContainerAttribute;
        }
    }
}
