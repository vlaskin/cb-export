﻿
using Charbucket.Common.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DBDeploymentTool
{
    internal sealed class DbDeployer
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly SqlConnectionStringBuilder _connectionStringBuilder;
        private readonly SqlConnectionStringBuilder _connectionStringBuilderMaster;

        public string Server { get; private set; }
        public string Db { get; private set; }
        public string User { get; private set; }
        public string Password { get; private set; }
        public string ScriptsPath { get; private set; }
        public bool IntegratedSecurity { get; private set; }
        public bool ContinueIfFailed { get; private set; }
        public bool DropDatabase { get; private set; }

        public DbDeployer(string server, string db, string user, string password, string scriptsPath, string integratedSecurity, string continueIfFailed, string dropDatabase)
        {
            ScriptsPath = scriptsPath;
            Password = password;
            User = user;
            Db = db;
            Server = server;

            bool b;
            Boolean.TryParse(integratedSecurity, out b);
            IntegratedSecurity = b;

            Boolean.TryParse(continueIfFailed, out b);
            ContinueIfFailed = b;

            Boolean.TryParse(dropDatabase, out b);
            DropDatabase = b;

            if (DropDatabase)
            {
                _connectionStringBuilderMaster = new SqlConnectionStringBuilder
                {
                    DataSource = Server,
                    InitialCatalog = "master",
                    IntegratedSecurity = IntegratedSecurity,
                    UserID = IntegratedSecurity ? "" : User,
                    Password = IntegratedSecurity ? "" : Password
                };
            }

            _connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = Server,
                InitialCatalog = Db,
                IntegratedSecurity = IntegratedSecurity,
                UserID = IntegratedSecurity ? "" : User,
                Password = IntegratedSecurity ? "" : Password
            };

        }

        public DbDeployer(IReadOnlyDictionary<string, string> parameters)
            : this(parameters["-server"], parameters["-db"], parameters["-user"], parameters["-password"], parameters["-path"], parameters["-integratedSecurity"], parameters["-continueIfFailed"], parameters["-dropDatabase"])
        {
        }

        public int Run()
        {
            var res = true;
            _logger.Info("DB deploy started.");
            _logger.Info("Connection string: '{0}'.", _connectionStringBuilder.ToString());

            if (DropDatabase)
            {
                DropDb();
            }

            var scripts = FindScriptsInDirectory(ScriptsPath);
            foreach (var script in scripts)
            {
                try
                {
                    RunScript(script);
                    _logger.Info("OK: {0}", script);
                }
                catch (Exception ex)
                {
                    _logger.Error("{0}", script);
                    res = false;
                    if (!ContinueIfFailed)
                    {
                        _logger.Error(ex);
                        break;
                    }
                }
            }

            return res ? 0 : -1;
        }

        private void DropDb()
        {
            var query = "ALTER DATABASE " + Db + @" SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
                         DROP DATABASE [" + Db + "];";

            new FluentSqlCommand()
                .WithConnection(_connectionStringBuilderMaster.ToString())
                .Query(query)
                .ExecuteNonQuery();

            new FluentSqlCommand()
                .WithConnection(_connectionStringBuilderMaster.ToString())
                .Query("CREATE DATABASE " + Db)
                .ExecuteNonQuery();
        }

        private static IEnumerable<string> FindScriptsInDirectory(string directory)
        {
            var scripts = new List<string>();

            var dirs = Directory.GetDirectories(directory);
            foreach (var dir in dirs)
            {
                scripts.AddRange(FindScriptsInDirectory(dir));
            }

            scripts.AddRange(Directory.GetFiles(directory, "*.sql"));

            return scripts.ToArray();
        }

        private void RunScript(string path)
        {
            var content = File.ReadAllText(path, Encoding.UTF8);

            var scripts = SplitSqlStatements(content);

            foreach (var script in scripts)
            {
                new FluentSqlCommand()
                    .WithConnection(_connectionStringBuilder.ToString())
                    .Query(script)
                    .ExecuteNonQuery();
            }
        }

        private static IEnumerable<string> SplitSqlStatements(string sqlScript)
        {
            // Split by "GO" statements
            var statements = Regex.Split(
                    sqlScript,
                    @"^\s*GO\s* ($ | \-\- .*$)",
                    RegexOptions.Multiline |
                    RegexOptions.IgnorePatternWhitespace |
                    RegexOptions.IgnoreCase);

            // Remove empties, trim, and return
            return statements
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Trim(' ', '\r', '\n'));
        }
    }
}
