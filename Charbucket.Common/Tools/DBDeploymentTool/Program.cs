﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace DBDeploymentTool
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static int Main(string[] args)
        {
            foreach (var a in args)
            {
                Logger.Info(a);
            }

            try
            {
                var parameters = new Dictionary<string, string>()
                {
                    {"-server", ""},
                    {"-db", ""},
                    {"-user", ""},
                    {"-password", ""},
                    {"-path", ""},
                    {"-integratedSecurity", ""},
                    {"-continueIfFailed", ""},
                    {"-dropDatabase", ""}
                };
                foreach (var p in args.Select(arg => arg.Split('=')))
                {
                    parameters[p[0]] = p[1];
                }

                var deployer = new DbDeployer(parameters);
                return deployer.Run();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return -1;
            }
        }
    }
}
