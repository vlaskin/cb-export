﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Charbucket.Business.Interfaces.Globalization.Models;

namespace Charbucket.Business.Core.Globalization.Models
{
    class LanguageModel : ILanguageModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
