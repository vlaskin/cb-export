﻿
using Charbucket.Business.Interfaces;
using Charbucket.Business.Interfaces.User.Models;
using System;

namespace Charbucket.Business.Core.User.Models
{
    internal class UserInfoModel : IUserInfoModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronyme { get; set; }
        public BlockStatus BlockStatus { get; set; }
        public DateTime LastLoginDatetime { get; set; }
        public string ImageThumbnail { get; set; }
        public string FullName { get; }
    }
}
