﻿using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Business.Interfaces.User.Models;

namespace Charbucket.Business.Core.User.Models
{
    internal class WorkAuthorModel : UserInfoModel, IWorkAuthorModel
    {
        public AuthorType AuthorType { get; set; }
    }
}
