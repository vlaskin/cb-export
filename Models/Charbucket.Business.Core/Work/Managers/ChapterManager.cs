﻿using System;
using Charbucket.Business.Interfaces.Work.Managers;
using Charbucket.Business.Interfaces.Work.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Charbucket.Business.Core.Work.Models;
using Charbucket.Business.Interfaces.Work.Enums;
using Charbucket.Common.Text.BbCode;
using Charbucket.Entities;

namespace Charbucket.Business.Core.Work.Managers
{
    public class ChapterManager : IChapterManager
    {
        /// <summary>
        /// Returns stub data
        /// </summary>
        /// <param name="workId"></param>
        /// <returns></returns>
        public IEnumerable<IChapterModel> GetForWork(int workId)
        {
            if (workId == 1)
            {
                var result = new List<IChapterModel>();
                
                //read stub data
                var files = Directory.GetFiles($"data/{workId}/", "chapter_*.txt");
                var i = 1;
                foreach (var file in files)
                {
                    result.Add(new ChapterModel(i)
                    {
                        Name = "Глава " + i,
                        Content = string.Empty,
                        ContentRaw = File.ReadAllText(file),
                        CreatedDatetime = DateTime.Now,
                        UpdatedDatetime = DateTime.Now,
                        Position = (short)(i+1),
                        UpdatedUserId = 1,
                        IsFree = true,
                        UserId = 1,
                        VisibilityStatus = VisibilityStatus.ForAll,
                        WorkId = 1
                    });
                    i++;
                }

                return result;
            }
            return new List<ChapterModel>();
        }

        #region ILayerManagerBase<IChapterModel>

        public IChapterModel Create(int id = 0)
        {
            return new ChapterModel(id);
        }

        public IChapterModel Read(int id)
        {
            throw new NotImplementedException();

        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Save(IChapterModel entity)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

