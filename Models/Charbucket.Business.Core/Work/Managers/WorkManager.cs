﻿using Charbucket.Business.Core.User.Models;
using Charbucket.Business.Core.Work.Models;
using Charbucket.Business.Interfaces;
using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Business.Interfaces.User.Models;
using Charbucket.Business.Interfaces.Work.Enums;
using Charbucket.Business.Interfaces.Work.Managers;
using Charbucket.Business.Interfaces.Work.Models;
using System;
using System.Collections.Generic;
using Charbucket.Business.Core.Globalization.Models;

namespace Charbucket.Business.Core.Work.Managers
{
    /// <summary>
    /// Заглушка, возвращающая тестовое произведение
    /// Можешь свободно менять возвращаемые данные, но старайся не менять интерфейс
    /// </summary>
    public class WorkManager : IWorkManager
    {

        #region ILayerManagerBase<IWorkModel>

        public IWorkModel Create(int id = 0)
        {
            return new WorkModel(id);
        }

        public IWorkModel Read(int id)
        {
            if (id == 1)
            {
                var genres = new List<IGenreModel>
                {
                    new GenreModel
                    {
                        Id = 1,
                        Name = "Философский роман",
                        Code = "phil_novel"
                    },
                    new GenreModel
                    {
                        Id = 1,
                        Name = "Психологический роман",
                        Code = "psy_novel"
                    }
                };
                var authors = new List<IWorkAuthorModel>
                {
                    new WorkAuthorModel
                    {
                        Id = 1,
                        BlockStatus = BlockStatus.Normal,
                        FirstName = "Фёдор",
                        LastName = "Достоевский",
                        AuthorType = AuthorType.Creator,
                        ImageThumbnail = "/user-image.jpeg"
                    }
                };

                return new WorkModel
                {
                    Id = id,
                    Name = "Записки из подполья",
                    Description = "Повесть Достоевского, изданная в 1864 году. Повествование ведётся от лица бывшего чиновника, который проживает в Петербурге. По своей проблематике предвещает идеи экзистенциализма.",
                    Language = new LanguageModel {Code = "ru", Name = "Русский", Id = 1},
                    AgeConstraint = AgeConstraint.ForAll,
                    BlockStatus = BlockStatus.Normal,
                    CreatedDatetime = DateTime.Now,
                    Form = new FormModel {Code = "novel", Name = "Роман", Id = 1},
                    LanguageId = 1,
                    LicenseType = LicenseType.AllRightsReserved,
                    PaymentType = PaymentType.Free,
                    Position = 0,
                    Price = 0,
                    SeriesId = 0,
                    UpdatedDatetime = DateTime.Now.AddHours(1),
                    UpdatedUserId = 1,
                    VisibiltyStatus = VisibilityStatus.ForAll,
                    WorkStatus = WorkStatus.Finished,
                    Image = "cover.jpeg",
                    ImageThumbnail = "cover-thumbnail.jpeg",
                    FormId = 1,
                    Authors = authors,
                    Genres = genres
                };
            }
            return null;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Save(IWorkModel entity)
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}
