﻿using Charbucket.Business.Interfaces.Work.Models;
using System;
using Charbucket.Business.Interfaces.Work.Enums;

namespace Charbucket.Business.Core.Work.Models
{
    internal class ChapterModel : IChapterModel
    {
        public int Id { get; }
        public int WorkId { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public DateTime UpdatedDatetime { get; set; }
        public int UpdatedUserId { get; set; }
        public VisibilityStatus VisibilityStatus { get; set; }
        public short Position { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string ContentRaw { get; set; }
        public bool IsFree { get; set; }

        public ChapterModel(int id)
        {
            Id = id;
        }
    }
}
