﻿using Charbucket.Business.Interfaces.Work.Models;

namespace Charbucket.Business.Core.Work.Models
{
    internal class FormModel : IFormModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
