﻿using Charbucket.Business.Interfaces;
using Charbucket.Business.Interfaces.Globalization.Models;
using Charbucket.Business.Interfaces.User.Models;
using Charbucket.Business.Interfaces.Work.Enums;
using Charbucket.Business.Interfaces.Work.Models;
using System;
using System.Collections.Generic;
using Charbucket.Business.Core.Globalization.Models;

namespace Charbucket.Business.Core.Work.Models
{
    internal class WorkModel : IWorkModel
    {
        public int Id { get; set; }
        public int? SeriesId { get; set; }
        public int? LanguageId { get; set; }
        public int? FormId { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public DateTime? UpdatedDatetime { get; set; }
        public int UpdatedUserId { get; set; }
        public short Position { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public WorkStatus WorkStatus { get; set; }
        public BlockStatus BlockStatus { get; set; }
        public VisibilityStatus VisibiltyStatus { get; set; }
        public PaymentType PaymentType { get; set; }
        public decimal? Price { get; set; }
        public AgeConstraint AgeConstraint { get; set; }
        public LicenseType LicenseType { get; set; }
        public int? CumulativeRating { get; }
        public int RatingCount { get; }
        public string Image { get; set; }
        public string ImageThumbnail { get; set; }
        
        public ILanguageModel Language { get; set; }
        public IFormModel Form { get; set; }
        public IEnumerable<IGenreModel> Genres { get; set; }
        public IEnumerable<IWorkAuthorModel> Authors { get; set; }

        public WorkModel(int id)
        {
            Id = id;
        }

        public WorkModel()
        {
        }
    }
}
