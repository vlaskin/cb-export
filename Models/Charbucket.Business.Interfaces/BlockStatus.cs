﻿
namespace Charbucket.Business.Interfaces
{
    public enum BlockStatus : byte
    {
        Normal = 0,
        Blocked = 1
    }
}
