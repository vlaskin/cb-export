﻿
namespace Charbucket.Business.Interfaces.Globalization.Models
{
    public interface ILanguageModel
    {
        int Id { get; set; }
        string Code { get; set; }
        string Name { get; set; }
    }
}
