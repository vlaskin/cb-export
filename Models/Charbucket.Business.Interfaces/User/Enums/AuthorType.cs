﻿namespace Charbucket.Business.Interfaces.User.Enums
{
    public enum AuthorType
    {
        Creator,
        CoAuthor
    }
}
