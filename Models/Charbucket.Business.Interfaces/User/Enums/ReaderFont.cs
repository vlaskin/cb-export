﻿namespace Charbucket.Business.Interfaces.User.Enums
{
    public enum ReaderFont : byte
    {
        OpenSans,
        Tahoma
    }
}
