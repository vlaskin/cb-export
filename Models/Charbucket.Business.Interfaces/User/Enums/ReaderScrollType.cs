﻿namespace Charbucket.Business.Interfaces.User.Enums
{
    public enum ReaderScrollType : byte
    {
        Scroll,
        OneColumnPage,
        TwoColumnPage
    }
}
