﻿namespace Charbucket.Business.Interfaces.User.Enums
{
    public enum ReaderTheme : byte
    {
        White,
        Sepia,
        Black
    }
}
