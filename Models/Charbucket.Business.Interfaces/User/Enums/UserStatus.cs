﻿namespace Charbucket.Business.Interfaces.User.Enums
{
    public enum UserStatus : byte
    {
        Standard,
        Old,
        Dead,
        Stub
    }
}
