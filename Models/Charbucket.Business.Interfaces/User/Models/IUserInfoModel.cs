﻿using System;

namespace Charbucket.Business.Interfaces.User.Models
{
    public interface IUserInfoModel
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Patronyme { get; set; }
        BlockStatus BlockStatus { get; set; }
        DateTime LastLoginDatetime { get; set; }
        string ImageThumbnail { get; set; }
    }
}
