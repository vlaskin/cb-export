﻿using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Business.Interfaces.User.Models;

namespace Charbucket.Business.Interfaces.User.Models
{
    public interface IWorkAuthorModel : IUserInfoModel
    {
        AuthorType AuthorType { get; set; }
        string FullName { get; }
    }
}
