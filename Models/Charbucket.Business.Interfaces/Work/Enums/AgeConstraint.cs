﻿namespace Charbucket.Business.Interfaces.Work.Enums
{
    public enum AgeConstraint : byte
    {
        NotSet,
        ForAll,
        For6,
        For12,
        For16,
        For18
    }
}
