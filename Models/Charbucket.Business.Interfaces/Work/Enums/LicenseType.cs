﻿namespace Charbucket.Business.Interfaces.Work.Enums
{
    public enum LicenseType: byte
    {
        NotSet,
        AllRightsReserved,
        PublicDomain,
        CreativeCommonsBy,
        CreativeCommonsBySa,
        CreativeCommonsByNd,
        CreativeCommonsByNc
    }
}
