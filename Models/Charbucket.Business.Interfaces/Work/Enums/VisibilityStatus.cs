﻿namespace Charbucket.Business.Interfaces.Work.Enums
{
    public enum VisibilityStatus : byte
    {
        ForMe,
        ForAll,
        ForList
    }
}
