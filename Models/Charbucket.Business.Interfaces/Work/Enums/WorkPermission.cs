﻿namespace Charbucket.Business.Interfaces.Work.Enums
{
    public enum WorkPermission : byte
    {
        Read,
        Review,
        Edit
    }
}
