﻿namespace Charbucket.Business.Interfaces.Work.Enums
{
    public enum WorkStatus : byte
    {
        Draft,
        Planned,
        Finished
    }
}
