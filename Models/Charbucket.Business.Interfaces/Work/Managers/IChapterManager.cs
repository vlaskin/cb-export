﻿using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Entities;
using System.Collections.Generic;

namespace Charbucket.Business.Interfaces.Work.Managers
{
    public interface IChapterManager : ILayerManagerBase<IChapterModel>
    {
        IEnumerable<IChapterModel> GetForWork(int workId);
    }
}
