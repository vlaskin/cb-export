﻿using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Entities;

namespace Charbucket.Business.Interfaces.Work.Managers
{
    public interface IWorkManager : ILayerManagerBase<IWorkModel>
    {

    }
}
