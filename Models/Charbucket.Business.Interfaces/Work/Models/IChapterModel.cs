﻿using System;
using Charbucket.Business.Interfaces.Work.Enums;

namespace Charbucket.Business.Interfaces.Work.Models
{
    public interface IChapterModel
    {
        int Id { get; }
        int WorkId { get; set; }
        int UserId { get; set; }
        DateTime CreatedDatetime { get; set; }
        DateTime UpdatedDatetime { get; set; }
        int UpdatedUserId { get; set; }
        VisibilityStatus VisibilityStatus { get; set; }
        short Position { get; set; }
        string Name { get; set; }
        string Content { get; set; }
        string ContentRaw { get; set; }
        bool IsFree { get; set; }
    }
}
