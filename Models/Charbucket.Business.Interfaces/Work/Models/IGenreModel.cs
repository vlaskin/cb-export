﻿namespace Charbucket.Business.Interfaces.Work.Models
{
    public interface IGenreModel
    {
        int Id { get; set; }
        string Code { get; set; }
        string Name { get; set; }
    }
}
