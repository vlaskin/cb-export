﻿using Charbucket.Business.Interfaces.Globalization.Models;
using Charbucket.Business.Interfaces.User.Models;
using Charbucket.Business.Interfaces.Work.Enums;
using System;
using System.Collections.Generic;

namespace Charbucket.Business.Interfaces.Work.Models
{
    public interface IWorkModel
    {
        int Id { get; }
        int? SeriesId { get; set; }
        int? LanguageId { get; set; }
        int? FormId { get; set; }
        DateTime CreatedDatetime { get; set; }
        DateTime? UpdatedDatetime { get; set; }
        int UpdatedUserId { get; set; }
        short Position { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        WorkStatus WorkStatus { get; set; }
        BlockStatus BlockStatus { get; set; }
        VisibilityStatus VisibiltyStatus { get; set; }
        PaymentType PaymentType { get; set; }
        decimal? Price { get; set; }
        AgeConstraint AgeConstraint { get; set; }
        LicenseType LicenseType { get; set; }
        int? CumulativeRating { get; }
        int RatingCount { get; }
        string Image { get; set; }
        string ImageThumbnail { get; set; }
        
        ILanguageModel Language { get; }
        IFormModel Form { get; }
        IEnumerable<IGenreModel> Genres { get; }
        IEnumerable<IWorkAuthorModel> Authors { get; }
    }
}
