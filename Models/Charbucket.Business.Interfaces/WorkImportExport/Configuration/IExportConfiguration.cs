﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charbucket.Business.Interfaces.WorkImportExport.Configuration
{
    public interface IExportConfiguration
    {
        string UserLanguage { get; set; }
    }
}
