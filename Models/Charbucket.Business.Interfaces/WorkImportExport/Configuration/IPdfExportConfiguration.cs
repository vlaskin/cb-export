﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Charbucket.Business.Interfaces.WorkImportExport.Configuration
{
    public interface IPdfExportConfiguration : IExportConfiguration
    {
        Rectangle PageSize { get; set; }
        Font content_font { get; set; } 
        Font main_header_font { get; set; }
        Font chapter_header_font{ get; set; }
    }
}
