﻿using Charbucket.Business.Interfaces.WorkImportExport.Configuration;

namespace Charbucket.Business.Interfaces.WorkImportExport
{
    public interface IWorkExportManager
    {
        string Export(int workId, WorkFormat format, IExportConfiguration config);
        IExportConfiguration GetExportConfig(WorkFormat format);
    }
}
