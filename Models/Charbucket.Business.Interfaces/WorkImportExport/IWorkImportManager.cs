﻿using System.IO;

namespace Charbucket.Business.Interfaces.WorkImportExport
{
    public interface IWorkImportManager
    {
        void Import(int workId, WorkFormat format, Stream stream);
    }
}
