﻿namespace Charbucket.Business.Interfaces.WorkImportExport
{
    public enum WorkFormat
    {
        Html,
        Fb2,
        Docx,
        Pdf,
        Txt,
        Epub
    }
}
