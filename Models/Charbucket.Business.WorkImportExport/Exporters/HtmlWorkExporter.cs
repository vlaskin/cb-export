﻿using System.Collections.Generic;
using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Business.WorkImportExport.Exporters.Interfaces;
using System.IO;
using Charbucket.Common.Extensions;
using System.Linq;
using System.Text.RegularExpressions;
using Charbucket.Common.Text.BbCode;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using System;

namespace Charbucket.Business.WorkImportExport.Exporters
{
    // для HTML чуть поинтереснее, чем для TXT
    // тут мы именно что должны выполнить перегонку служебных тегов в теги HTML - [b] -> <b>, [url=http://wikipedia.com]Wiki[/url] -> <a href="http://wikipedia.com">Wiki</a> и так далее
    // важно, чтобы тут остался простор для расширения - 100% вероятности, что появятся новые служебные теги, которые нужно будет поддерживать в этом (и во всех остальных) экспортере
    // для HTML-документа мы уже можем использовать кое-какое форматирование, вплоть до создания встраиваемого CSS
    // пока скорее всего не нужно, но опять же, стоит предусмотреть
    class HtmlWorkExporter : IWorkExporter
    {
        #region Fiealds and Properties
        private static readonly List<Regex> regexList;
        private static readonly List<string> head_tags; //style's tags should be here
        public string Extension => "html";
        #endregion

        #region Initialization
        static HtmlWorkExporter()
        {    
            regexList = new List<Regex>
            {
                new Regex(@"\[url.*?\](?<link_body>.*?)\[/url\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase),// [url]
                new Regex("^(.*)$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline), // new line
                new Regex(@"\[(b)\](.*?)\[/(b)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [b]
                new Regex(@"\[(i)\](.*?)\[/(i)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [i]
                new Regex(@"\[(u)\](.*?)\[/(u)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [u]
                new Regex(@"\[(s)\](.*?)\[/(s)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [s]
                
            };

            head_tags = null;
        }
        #endregion

        #region  Export
        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels)
        {
            
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream, new System.Text.UTF8Encoding(true, true));
            
            CreateHeader(streamWriter, workModel, head_tags);
            CreateBody(streamWriter, chapterModels);
            streamWriter.Close();

            return memoryStream.ToArray();            
        }
        #endregion

        #region CreateHeader
        private static void CreateHeader(TextWriter sw, IWorkModel workModel, IEnumerable<string> head_tags = null)
        {
            sw.WriteLine("<!DOCTYPE html>");
            sw.WriteLine("<html>");
            sw.WriteLine("<head>");
            sw.WriteLine("<h1>");
            sw.WriteLine(workModel.Name.ToUpper());
            sw.WriteLine("</h1>");
            sw.WriteLine("<title>");
            sw.WriteLine(workModel.Name.ToUpper()); //or smth else
            sw.WriteLine("</title>");

            //adding style's tags, if necessary
            if (head_tags != null)
            {
                foreach(var tag in head_tags)
                {
                    sw.WriteLine(tag);
                }
            }
            sw.WriteLine("<br>");
            
            var author = workModel.Authors.FirstOrDefault(a => a.AuthorType == Business.Interfaces.User.Enums.AuthorType.Creator);
            if (author != null)
            {
                sw.WriteLine($"{author.FirstName} {author.LastName} (http://charbucket.com/u{author.Id})");
            }
            sw.WriteLine("<br>");

            sw.WriteLine("Дата создания: {0}", workModel.CreatedDatetime); //вот эти все строки и шаблоны будут храниться в ресурсах, по ним не заморачивайся
            if (workModel.CreatedDatetime != workModel.UpdatedDatetime)
            {
                sw.WriteLine("<br>");
                sw.WriteLine("Дата редактирования: {0}", workModel.UpdatedDatetime);
            }
            sw.WriteLine("<br>");
            
            string description = workModel.Description;
            sw.WriteLine(parser(ref description));
            
            sw.WriteLine("</head>");
            sw.WriteLine("<br>");
        }
        #endregion

        #region CreateBody
        private static void CreateBody(TextWriter sw, IEnumerable<IChapterModel> chapterModels)
        {
            sw.WriteLine("<body>");
            foreach (var chapterModel in chapterModels.OrderBy(c => c.Position))
            {
                var chapterHeader = chapterModel.Name.IsNullOrEmpty() ? chapterModel.Position.ToString() : chapterModel.Name;
                sw.WriteLine("<h2>");
                sw.WriteLine($"- {chapterHeader} -");
                sw.WriteLine("</h2>");
                sw.WriteLine("<br>");
                //sw.WriteLine();                
                
                var content = chapterModel.ContentRaw; // в ContentRaw хранится BbCode-текст, с ним и работа
                
                sw.WriteLine(parser(ref content));
                
            }
            sw.WriteLine("</body>");
            sw.WriteLine("</html>");
        }
        #endregion

        #region  parser
        /// <summary>
        /// the main function for converting bbcode to html
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string parser(ref string input)
        {
            for (int i = 0; i < regexList.Count; i++)
            {
                switch (i)
                {
                    case 0:  //for [url] tag
                        input = regexList[i].Replace(input, "<a href = \"${link_body}\">${link_body}</a>");
                        break;
                    case 1:  // for new lines, add <p></p>
                        input = regexList[i].Replace(input, "<p>$1</p>\n\r");
                        break;
                    default:  //for other tags
                        input = regexList[i].Replace(input, "<$1>$2</$1>");
                        break;
                }
            }

            return input;
        }

        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels, IExportConfiguration _config)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
