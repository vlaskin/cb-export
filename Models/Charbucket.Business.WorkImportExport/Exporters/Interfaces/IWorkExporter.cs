﻿using System.Collections.Generic;
using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Charbucket.Business.WorkImportExport.Exporters.Interfaces
{
    /// <summary>
    /// Базовый интерфейс для всех экспортеров
    /// Как видно из интерфейса, каждый экспортер обязан уметь сформировать на основе моделей какой-либо полезный массив байт
    /// Тут фишка в том, что основная логика, которую придётся написать, должна находиться внутри конкретного экспортера
    /// Этот самый экспортер может делать всё что угодно с данными (хоть писать их в темповый файл, мб для PDF это и пригодится), главное - вернуть результат.
    /// Всю временную информацию, оставшуюся после отработки, нужно разумеется подчистить (ну, например удалить временный файл)
    /// </summary>
    internal interface IWorkExporter
    {
        //вот это свойство немного выбивается из принципа single responsibility, потому что экспортеру должно быть поебать, в какое расширение WorkExportManager запишет результат его работы
        //но похуй :)
        string Extension { get; }

        //основной метод, в котором будет происходить работа по экспорту
        byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels, IExportConfiguration _config);
        
    }
}
