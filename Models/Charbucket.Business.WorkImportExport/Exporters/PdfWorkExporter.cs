﻿using Charbucket.Business.Interfaces.Work.Models;
using System;
using System.Collections.Generic;
using Charbucket.Business.WorkImportExport.Exporters.Interfaces;
using Charbucket.Common.Text.BbCode;
using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Common.Extensions;
using System.Linq;
using System.Text.RegularExpressions;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;


namespace Charbucket.Business.WorkImportExport.Exporters
{
    internal class PdfWorkExporter : IWorkExporter
    {
        public string Extension => "pdf";

        static readonly List<Regex> regexList;         
        static readonly string newline;  // simple new line 
        
        static PdfWorkExporter()
        {
            newline = Environment.NewLine;
                     
            //regex denfinition
            regexList = new List<Regex>
            {
                new Regex(@"\[(b)\](.*?)\[/(b)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [b]
                new Regex(@"\[(i)\](.*?)\[/(i)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [i]
                new Regex(@"\[(u)\](.*?)\[/(u)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [u]
                new Regex(@"\[(s)\](.*?)\[/(s)\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [s]
                new Regex(@"\[url.*?\](?<link_body>.*?)\[/url\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase),// [url]
                new Regex("^(.*)$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline), // new line
            };
            
        }

        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels, IExportConfiguration _config)
        {
            var config = _config as IPdfExportConfiguration;           

            var memoryStream = new MemoryStream();
            
            Document doc = new Document(config.PageSize);
            var userLanguage = _config.UserLanguage;          
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            CreateHeader(doc, workModel, config);
            CreateBody(doc, chapterModels, config);
            doc.Close();
            pdfWriter.Close();   
            return memoryStream.ToArray();
        }

        private static void CreateHeader(Document doc, IWorkModel workModel, IPdfExportConfiguration config)
        {
            
            doc.Add(new Paragraph(workModel.Name.ToUpper(), config.main_header_font));

            var author = workModel.Authors.FirstOrDefault(a => a.AuthorType == AuthorType.Creator);
            if (author != null)
            {
                doc.Add(new Paragraph($"{author.FirstName} {author.LastName} (http://charbucket.com/u{author.Id})",config.chapter_header_font));
            }
            
            doc.Add(new Paragraph($"Дата создания: {workModel.CreatedDatetime}", config.content_font)); //вот эти все строки и шаблоны будут храниться в ресурсах, по ним не заморачивайся
            if (workModel.CreatedDatetime != workModel.UpdatedDatetime)
            {
                doc.Add(new Paragraph($"Дата редактирования: {workModel.UpdatedDatetime}",config.content_font) );
            }

            doc.Add(new Paragraph(newline));
            doc.Add(new Paragraph(workModel.Description, config.content_font));
            doc.Add(new Paragraph(newline));
        }
       
        private static void CreateBody(Document doc, IEnumerable<IChapterModel> chapterModels, IPdfExportConfiguration config)
        {
            foreach (var chapterModel in chapterModels.OrderBy(c => c.Position))
            {
                var chapterHeader = chapterModel.Name.IsNullOrEmpty() ? chapterModel.Position.ToString() : chapterModel.Name;
                doc.Add(new Paragraph(newline));
                doc.Add(new Paragraph($"- {chapterHeader} -", config.chapter_header_font));
                doc.Add(new Paragraph(newline));

                var content = chapterModel.ContentRaw; // в ContentRaw хранится BbCode-текст, с ним и работа 
                doc.Add(new Paragraph(content, config.content_font));
            }
        }
    }
}
