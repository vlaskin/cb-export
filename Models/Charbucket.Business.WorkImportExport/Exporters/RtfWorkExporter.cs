﻿using Charbucket.Business.Interfaces.Work.Models;
using System;
using System.Collections.Generic;
using Charbucket.Business.WorkImportExport.Exporters.Interfaces;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;

namespace Charbucket.Business.WorkImportExport.Exporters
{
    internal class RtfWorkExporter : IWorkExporter
    {
        public string Extension => "rtf";
        

        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels, IExportConfiguration _config)
        {
            throw new NotImplementedException();
        }
    }
}
