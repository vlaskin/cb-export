﻿using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Business.WorkImportExport.Exporters.Interfaces;
using Charbucket.Common.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Charbucket.Common.Text.BbCode;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using System;

namespace Charbucket.Business.WorkImportExport.Exporters
{
    // пример экспортера
    // этот класс выполняет всю работу по перегонке текста из внутреннего формата в необходимый, возвращая менеджеру массив байтов
    // эти классы можно переписывать по своему успотрению, при необходимости можешь менять интерфейс, только без фанатизма
    // внутри каждой реализации экспортера можешь творить всё, что хочешь, главное, чтобы вышестоящие чуваки не знали деталей реализации того или иного метода
    internal class TxtWorkExporter : IWorkExporter
    {
        private static readonly List<Regex> regexList;

        static TxtWorkExporter()
        {
            // так как формат TXT вообще не поддерживает никакого форматирования - просто вырезаем все BB-теги
            regexList = new List<Regex>
            {
                new Regex(@"\[b\](.*?)\[/b\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), //один из поддерживаемых тегов - [b]
                new Regex(@"\[i\](.*?)\[/i\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase), // [i]
                new Regex(@"\[url.*?\](.*?)\[/url\]", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase) // [url]
            };
        }

        public string Extension => "txt";
        
        // опять же, это просто тупой пример
        // логика в том, что произведение в формате TXT - это просто кусок текста
        // поэтому формируем тупой заголовок и тупо формируем контент, вырезая внутренние конструкции форматирования
        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels)
        {
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);

            CreateHeader(streamWriter, workModel);
            CreateBody(streamWriter, chapterModels);
            streamWriter.Close();

            return memoryStream.ToArray();
        }

        // как правило, сначала в произведении идёт какая-то общая инфа: заголовок, имя автора, аннотация и прочее
        // так как это txt, тут мы не мудрствуем лукаво, а просто вываливаем это как есть
        private static void CreateHeader(TextWriter sw, IWorkModel workModel)
        {
            sw.WriteLine(workModel.Name.ToUpper());

            var author = workModel.Authors.FirstOrDefault(a => a.AuthorType == AuthorType.Creator);
            if (author != null)
            {
                sw.WriteLine($"{author.FirstName} {author.LastName} (http://charbucket.com/u{author.Id})");
            }
            sw.WriteLine("Дата создания: {0}", workModel.CreatedDatetime); //вот эти все строки и шаблоны будут храниться в ресурсах, по ним не заморачивайся
            if (workModel.CreatedDatetime != workModel.UpdatedDatetime)
            {
                sw.WriteLine("Дата редактирования: {0}", workModel.UpdatedDatetime);
            }

            sw.WriteLine();
            sw.WriteLine(workModel.Description);
            sw.WriteLine();
            
        }

        // для основного контента книги тоже всё относительно просто
        // пробегаем по всем главам, вырезаем служебные теги (только для txt, в остальных форматах нужно будет например перегонять внутренние теги в теги того стандарта, в который перегоняем), пишем результат
        // например тег [b][/b] может соответствовать HTML тегу <b></b>. а может и не соответствовать :)
        private static void CreateBody(TextWriter sw, IEnumerable<IChapterModel> chapterModels)
        {
            foreach (var chapterModel in chapterModels.OrderBy(c=>c.Position))
            {
                var chapterHeader = chapterModel.Name.IsNullOrEmpty() ? chapterModel.Position.ToString() : chapterModel.Name;
                sw.WriteLine();
                sw.WriteLine($"- {chapterHeader} -");
                sw.WriteLine();

                var content = chapterModel.ContentRaw; // в ContentRaw хранится BbCode-текст, с ним и работа
                regexList.ForEach(r =>
                {
                    // просто вырезаем тег
                    content = r.Replace(content, "$1");
                });

                sw.WriteLine(content);
            }
        }

        public byte[] Export(IWorkModel workModel, IEnumerable<IChapterModel> chapterModels, IExportConfiguration _config)
        {
            throw new NotImplementedException();
        }
    }
}
