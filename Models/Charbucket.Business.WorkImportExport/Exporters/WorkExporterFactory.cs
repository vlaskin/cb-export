﻿using Charbucket.Business.Interfaces.WorkImportExport;
using Charbucket.Business.WorkImportExport.Exporters.Interfaces;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using System;

namespace Charbucket.Business.WorkImportExport.Exporters
{
    /// <summary>
    /// Тупая фабрика для получения нужного экспортера
    /// </summary>
    internal static class WorkExporterFactory
    {
        public static IWorkExporter GetWorkExporter(WorkFormat format)
        {
            switch (format)
            {
                case WorkFormat.Docx:
                    return new DocxWorkExporter();
                case WorkFormat.Epub:
                    return new EpubWorkExporter();
                case WorkFormat.Fb2:
                    return new Fb2WorkExporter();
                case WorkFormat.Html:
                    return new HtmlWorkExporter();
                case WorkFormat.Pdf:
                    return new PdfWorkExporter();
                case WorkFormat.Txt:
                    return new TxtWorkExporter();
                default:
                    throw new NotImplementedException();
            }
        }
        
    }
}
