﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace Charbucket.Business.WorkImportExport.ExportersConfig
{
    class PdfExportConfig : IPdfExportConfiguration
    {

        static string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
        static BaseFont baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        public Font chapter_header_font
        {
            get
            {
                return new Font(baseFont, 20);
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Font content_font
        {
            get
            {
                return new Font(baseFont, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL);
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Font main_header_font
        {
            get
            {
                return new Font(baseFont, 25);
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Rectangle PageSize
        {
            get
            {
                return iTextSharp.text.PageSize.A4;
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string UserLanguage
        {
            get
            {
                return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
