﻿using Charbucket.Business.Interfaces.User.Enums;
using Charbucket.Business.Interfaces.Work.Managers;
using Charbucket.Business.Interfaces.Work.Models;
using Charbucket.Business.Interfaces.WorkImportExport;
using Charbucket.Business.WorkImportExport.Exporters;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;
using Charbucket.Business.WorkImportExport.ExportersConfig;
using System;
using System.IO;
using System.Linq;
using System.Threading;



namespace Charbucket.Business.WorkImportExport
{
    /// <summary>
    /// Менеджер, выполняющий общую работу по экпорту произведений
    /// Старайся особо не менять интерфейс, только по необходимости
    /// </summary>
    public class WorkExportManager : IWorkExportManager
    {
        private readonly IWorkManager _workManager; // менеджеры, нужные для работы, приходят через DI в конструктор
        private readonly IChapterManager _chapterManager;

        public WorkExportManager(IWorkManager workManager, IChapterManager chapterManager)
        {
            _workManager = workManager;
            _chapterManager = chapterManager;
        }

        
            

        public string Export(int workId, WorkFormat format, IExportConfiguration config)
        {
            // запрашиваем у фабрики экспортеров нужный
            var exporter = WorkExporterFactory.GetWorkExporter(format);

            //получаем метаданные произведения
            var work = _workManager.Read(workId);
            //получаем главы
            var chapters = _chapterManager.GetForWork(workId);

            //вычисляем имя итогового файла
            var fileName = GetFileName(work, exporter.Extension);

            //получаем нужный конфиг
            var _config = GetExportConfig(format);

            //логика в том, чтобы всегда отдавать ссылку на самый последний файл (учитываем изменения сущности в DB и дату модификации файла)
            if (IsFileUpToDate(fileName, work.UpdatedDatetime))
            {
                //собсно, запускаем процесс экспорта в случае устаревшей версии
                var content = exporter.Export(work, chapters, _config);
                WriteFile(fileName, content);
            }

            return fileName;
        }

        private static bool IsFileUpToDate(string filename, DateTime? updatedDatetime)
        {
            var fileInfo = new FileInfo(filename);
            return !fileInfo.Exists || (fileInfo.LastWriteTime < updatedDatetime);
        }

        private static string GetFileName(IWorkModel workModel, string extension)
        {
            var author = workModel.Authors.FirstOrDefault(a => a.AuthorType == AuthorType.Creator);
            var authorPath = string.Empty;
            if (author != null)
            {
                authorPath = $@"\{author.Id}\";
            }

            // тут мы учитываем текущую локаль приложения (пользователя) для шаблонной информации (всяких общих строк и прочего)
            // по этому поводу можешь не заморачиваться
            var userLanguage = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            var path = $"{workModel.Id}.{userLanguage}.{extension}";
            var fullPath = Path.Combine(authorPath, path);
            return "export" + fullPath; //опять же, я захардкодил все строки, в итоговой версии здесь будут данные из конфигурации. просто чтоб ты не отвлекался на рутинные вещи
        }

        //возрвращает нужный конфиг
        public IExportConfiguration GetExportConfig(WorkFormat format)
        {
            switch (format)
            {
                case WorkFormat.Docx:

                case WorkFormat.Epub:

                case WorkFormat.Fb2:

                case WorkFormat.Txt:

                case WorkFormat.Pdf:
                    return new PdfExportConfig();

                default:
                    throw new NotImplementedException();
            }
        }
        

        private static void WriteFile(string fileName, byte[] content)
        {
            
            File.WriteAllBytes(fileName, content);            
        }
    }
}
