﻿namespace Charbucket.Entities
{
    public sealed class GenericPermission<TPermissionType>
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public TPermissionType Permission { get; set; }
    }
}
