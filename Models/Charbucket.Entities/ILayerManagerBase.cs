﻿namespace Charbucket.Entities
{
    public interface ILayerManagerBase<TEntity>
    {
        TEntity Create(int id = 0);
        TEntity Read(int id);
        void Delete(int id);
        int Save(TEntity entity);
    }
}
