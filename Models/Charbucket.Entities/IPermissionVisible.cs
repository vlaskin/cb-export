﻿using System.Collections.Generic;

namespace Charbucket.Entities
{
    public interface IPermissionVisible<TPermissionEnum>
    {
        void SavePermissions(int id, IEnumerable<int> userIds);
        IEnumerable<GenericPermission<TPermissionEnum>> GetPermissions(int id);
    }
}
