﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charbucket.Entities
{
    public enum SecureAccessLevel : byte
    {
        Deny,
        AllowRead,
        AllowEdit,
    }
}
