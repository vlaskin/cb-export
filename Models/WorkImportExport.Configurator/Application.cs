﻿using Charbucket.Common.Logging;

namespace WorkImportExport.Configurator
{
    public static class Application
    {
        private static readonly object SyncRoot = new object();
        private static bool _isInitialized;
        private static readonly Log Log = Log.For("Application");

        public static void Initialize()
        {
            lock (SyncRoot)
            {
                if (_isInitialized) return;

                InitializeDataFactory();
                InitializeBusinessFactory();

                _isInitialized = true;
            }
        }

        private static void InitializeBusinessFactory()
        {
            BusinessFactory.Init();
            Log.Debug("Business factory initialized.");
        }

        private static void InitializeDataFactory()
        {
            DataFactory.Init();
            Log.Debug("Data factory initialized.");
        }
    }
}
