﻿using Charbucket.Business.Core.Work.Managers;
using Charbucket.Business.Interfaces.Work.Managers;
using Charbucket.Business.Interfaces.WorkImportExport;
using Charbucket.Business.WorkImportExport;
using Charbucket.Common.IoC;

namespace WorkImportExport.Configurator
{
    internal class BusinessFactory : FactoryBase
    {
        private static BusinessFactory MainInstance;
        private static readonly object SyncRoot = new object();

        public static void Init()
        {
            lock (SyncRoot)
            {
                if (MainInstance != null)
                {
                    return;
                }
                MainInstance = new BusinessFactory();
                MainInstance.Initialize();
            }
        }

        /// <summary>
        /// Initialize DI
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            RegisterType<IWorkManager, WorkManager>();
            RegisterType<IChapterManager, ChapterManager>();
            RegisterType<IWorkExportManager, WorkExportManager>();
        }
    }
}
