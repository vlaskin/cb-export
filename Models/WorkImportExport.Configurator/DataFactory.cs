﻿using Charbucket.Common.IoC;

namespace WorkImportExport.Configurator
{
    internal class DataFactory : FactoryBase
    {
        private static DataFactory MainInstance;
        private static readonly object SyncRoot = new object();

        public static void Init()
        {
            lock (SyncRoot)
            {
                if (MainInstance != null)
                {
                    return;
                }
                MainInstance = new DataFactory();
                MainInstance.Initialize();
            }
        }

        protected override void Initialize()
        {
            base.Initialize();
        }
    }
}
