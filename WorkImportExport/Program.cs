﻿using Charbucket.Business.Interfaces.WorkImportExport;
using Charbucket.Common.IoC;
using Charbucket.Common.Logging;
using WorkImportExport.Configurator;
using Charbucket.Business.Interfaces.WorkImportExport.Configuration;

namespace WorkImportExport
{
    /// <summary>
    /// Точка входа для дебага
    /// Можешь свободно менять на своё усмотрение
    /// </summary>
    class Program
    {
        private static readonly Log Logger = Log.For<Program>();

        static void Main(string[] args)
        {
            Logger.Debug("start");

            // инициализация IoC-контейнеров (Inversion of control) для Dependency Injection (DI)
            // очень распространённая и полезная штука
            // суть в том, чтобы переложить контроль за созданием экземпляров ключевых классов со всего кода на специализированный изолированный участок
            // там происходит регистрация, а клиентское приложение может по мере надобности запрашивать новые экземпляры
            // либо вообще получать их через конструкторы, явно нигде не вызывая new. интересная и полезная штука, почитай подробнее про неё, если время будет
            Application.Initialize();

            // получение из контейнера экземпляра менеджера
            var workExporter = FactoryBase.Instance.Create<IWorkExportManager>();

            // запрос ссылки на файл в нужном форматеваываы
            // экспорт происходит не всегда, мы кешируем результат

            //var fullFileName = workExporter.Export(1, WorkFormat.Txt);
            //var fullFileName2 = workExporter.Export(1, WorkFormat.Html);
            IExportConfiguration pdfCfg = workExporter.GetExportConfig(WorkFormat.Pdf);
            
            var fullFileName3 = workExporter.Export(1, WorkFormat.Pdf, pdfCfg);
        }
    }
}
